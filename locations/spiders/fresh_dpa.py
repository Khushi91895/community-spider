import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class FreshSpider(scrapy.Spider):
    name = "fresh_dpa"
    brand_name = "Fresh"
    spider_type = "chain"
    spider_chain_id = "5921"
    spider_categories = [Code.GROCERY.value]
    spider_countries = [pycountry.countries.lookup('svk').alpha_3]
    allowed_domains = ["freshobchod.sk", "webapi.freshweb.anovative.com"]
    
    # start_urls = ['https://freshobchod.sk']

    def start_requests(self):
        yield scrapy.Request(
            url='https://webapi.freshweb.anovative.com/predajne',
            callback=self.parse
        )

    def parse(self, response):
        '''
        @url https://webapi.freshweb.anovative.com/predajne
        @returns items 775 790
        @scrapes ref name addr_full street city country phone opening_hours website email lat lon
        '''
        data = response.json()

        for row in data:

            data = {
                'ref': row.get('id'),
                'chain_id': "5921",
                'chain_name': "Fresh",
                'addr_full': row.get('ul'),
                'city': row.get('ob'),
                'phone': row.get('tel'),
                'opening_hours': [f"Mo - Sa {row.get('ot')}"],
                'website': 'https://www.freshobchod.sk/',
                'email': ["fresh@freshobchod.sk"],
                'lat': float(row.get('lat')),
                'lon': float(row.get('lon')),
            }
            
            yield GeojsonPointItem(**data)