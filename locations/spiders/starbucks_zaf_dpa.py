import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from datetime import datetime, timedelta


class StarbucksZAF(scrapy.Spider):
    name = "starbucks_zaf_dpa"
    brand_name = 'Starbucks'
    spider_type = 'chain'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('zaf').alpha_3]
    min_lat, max_lat = -34.8151256, -22.1254796
    min_lng, max_lng = 16.4460000, 32.8920000
    step = 0.1

    # start_urls = ["https://www.starbucks.co.za"]

    def start_requests(self):
        base_url = "https://www.starbucks.co.za/api/v1/store-finder?latLng="
        # yield scrapy.Request(url=url, callback=self.coordcycle)
        for lat in range(int(self.min_lat/self.step), int(self.max_lat/self.step)):
            for lng in range(int(self.min_lng/self.step), int(self.max_lng/self.step)):
                url = base_url+f"{lat*self.step}%2C{lng*self.step}"
                yield scrapy.Request(url=url, callback=self.parse)

    def get_day_abbr(name: str) -> str:
        today = datetime.date.today()
        tomorrow = today + datetime.timedelta(days=1)

        if name == 'Today':
            return days.get(today.strftime('%A'))
        elif name == 'Tomorrow':
            return days.get(tomorrow.strftime('%A'))
        else:
            return days.get(name)

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
            data = {
                'ref': row['id'],
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': row['address'],
                'phone': row['phoneNumber'],
                'lat': float(row['coordinates']['lat']),
                'lon': float(row['coordinates']['lng']),
                'website': 'https://www.starbucks.co.za',
                "opening_hours": '',
            }
            days = {
                'Monday': 'Mo',
                'Tuesday': 'Tu',
                'Wednesday': 'We',
                'Thursday': 'Th',
                'Friday': 'Fr',
                'Saturday': 'Sa',
                'Sunday': 'Su',
            }

            opening_hours = ''

            for hours in row['hoursNext7Days']:
                name = hours['name']
                if name == 'Today':
                    name = self.get_day_abbr()
                elif name == 'Tomorrow':
                    name = self.get_day_abbr(1)
                else:
                    name = days.get(name, '')

                if name:
                    start_time, end_time = hours['description'].split(' to ')
                    start_time = datetime.strptime(
                        start_time, '%I:%M %p').strftime('%H:%M')
                    end_time = datetime.strptime(
                        end_time, '%I:%M %p').strftime('%H:%M')
                    opening_hours += f'{name} {start_time}-{end_time}; '

            data['opening_hours'] = opening_hours.strip()

            yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=0):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days % 7]
