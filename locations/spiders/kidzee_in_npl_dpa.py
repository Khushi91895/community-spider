import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry
import uuid


class KidzeeSpider(scrapy.Spider):
    name = 'kidzee_in_npl_dpa'
    brand_name = 'Kidzee'
    spider_type = 'chain'
    spider_chain_id = '28405'
    spider_categories = [Code.SCHOOL.value]
    spider_countries = [
        pycountry.countries.lookup('in').alpha_3,
        pycountry.countries.lookup('nepal').alpha_3
        ]
    allowed_domains = ['www.kidzee.com']
    
    # start_urls = ['https://www.kidzee.com/WebRoute/GetFranchiseedetailsStateCitywise']

    def start_requests(self):
        yield scrapy.Request(
            url="https://www.kidzee.com/contact-us/",
            method="GET",
            callback=self.parse_contact
        )
    
    def parse_contact(self, response):
        opening_hours = response.xpath("/html/body/footer/div[1]/div/div[4]/div/div/span/span/text()").get()
        email = response.xpath('//*[@id="femail"]/text()').get()
        phone = response.xpath('/html/body/footer/div[1]/div/div[2]/div/div/span/a/text()').get()

        countries = ["India", "Nepal"]

        for country in countries:
            yield scrapy.FormRequest(
                url="https://www.kidzee.com/WebRoute/GetFranchiseedetailsStateCitywise",
                method="POST",
                formdata={"Search": country},
                cb_kwargs=dict(
                    opening_hours=opening_hours, 
                    phone=phone, 
                    email=email
                    ),
                callback=self.parse
            )

    def parse(self, response, opening_hours, phone, email):
        '''
        @url https://www.kidzee.com/WebRoute/GetFranchiseedetailsStateCitywise
        @returns items 50 70
        '''
        responseData = response.json()

        for item in responseData:          
            store = {
                'ref': uuid.uuid4().hex,
                'chain_name': 'Kidzee',
                'chain_id': "28405",
                'website': 'https://www.kidzee.com',
                'street': item.get("Address1", ""),
                'city': item.get("City_Name", ""),
                'state': item.get("State_Name", ""),
                'opening_hours': opening_hours,
                'email': [
                    email,
                    item.get("Email_Id", "")
                ],
                'phone': [
                    phone,
                    item.get("Mobile_No", ""),
                    item.get("Phone_No1", ""),
                ],
                'lat': item.get("Latitude", ""),
                'lon': item.get("Longitude", ""),
            }

            yield GeojsonPointItem(**store)
