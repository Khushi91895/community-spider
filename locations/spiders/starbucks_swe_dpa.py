import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from datetime import datetime, timedelta


class StarbucksSWE(scrapy.Spider):
    name = "starbucks_swe_dpa"
    spider_type = 'chain'
    brand_name = 'Starbucks'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('swe').alpha_3]
    min_lat, max_lat = 55.2000001, 69.1000001
    min_lng, max_lng = 10.5000001, 24.2000001
    step = 0.1

    # start_urls = ["https://www.starbucks.se"]

    def start_requests(self):
        base_url = "https://www.starbucks.se/sv/api/v1/store-finder?latLng="
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                if (self.is_inside_country_polygon(lat * self.step, lng * self.step)):
                    url = base_url + f"{lat * self.step}%2C{lng * self.step}"
                    yield scrapy.Request(url=url, callback=self.parse)

    def is_inside_country_polygon(self, lat, lng):
        country_hexagon_vertices = [
            (55.409918, 12.987017),
            (56.213375, 12.793999),
            (58.596747, 11.324048),
            (59.526546, 12.358912),
            (61.703138, 13.301950),
            (66.460961, 16.281211),
            (68.420393, 20.595200),
            (65.941995, 23.670168),
            (59.249705, 18.549454),
            (55.447730, 14.201950),
        ]

        n = len(country_hexagon_vertices)
        inside = False
        p1x, p1y = country_hexagon_vertices[0]
        for i in range(n + 1):
            p2x, p2y = country_hexagon_vertices[i % n]
            if lng > min(p1y, p2y):
                if lng <= max(p1y, p2y):
                    if lat <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (lng - p1y) * (p2x - p1x) / \
                                (p2y - p1y) + p1x
                        if p1x == p2x or lat <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
            if row['description'] != 'Closed':
                data = {
                    'ref': row['id'],
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'addr_full': row['address'],
                    'phone': row['phoneNumber'],
                    'website': 'https://www.starbucks.se',
                    'lat': float(row['coordinates']['lat']),
                    'lon': float(row['coordinates']['lng']),
                    "opening_hours": '',
                }
                days = {
                    'Monday': 'Mo',
                    'Tuesday': 'Tu',
                    'Wednesday': 'We',
                    'Thursday': 'Th',
                    'Friday': 'Fr',
                    'Saturday': 'Sa',
                    'Sunday': 'Su',
                }

                opening_hours = ''

                for hours in row['hoursNext7Days']:
                    name = hours['name']
                    if name == 'Today':
                        name = self.get_day_abbr()
                    elif name == 'Tomorrow':
                        name = self.get_day_abbr(1)
                    else:
                        name = days.get(name, '')
                    if (name and hours['description'] != 'Closed'):
                        start_time, end_time = hours['description'].split(
                            ' to ')
                        start_time = datetime.strptime(
                            start_time, '%I:%M %p').strftime('%H:%M')
                        end_time = datetime.strptime(
                            end_time, '%I:%M %p').strftime('%H:%M')
                        opening_hours += f'{name} {start_time}-{end_time}; '

                data['opening_hours'] = opening_hours.strip()
                yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=0):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days]
