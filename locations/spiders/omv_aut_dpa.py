import scrapy
import pycountry
import json
import urllib
from locations.items import GeojsonPointItem
from locations.categories import Code


class OMV_AUT(scrapy.Spider):
    name = "omv_aut_dpa"
    brand_name = 'OMV'
    spider_type = 'chain'
    spider_chain_id = "286"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('aut').alpha_3]

    def start_requests(self):
        url = "https://app.wigeogis.com/kunden/omv/data/getresults.php"
        form_data = {
            'CTRISO': 'AUT',
            'BRAND': 'OMV',
            'VEHICLE': 'CAR',
            'MODE': 'NEXTDOOR',
            'ANZ': '5',
            'HASH': 'ab8268befd66c674247ab188a1649244d6ec7880',
            'TS': '1686227327',
        }
        encoded_form_data = urllib.parse.urlencode(form_data)
        yield scrapy.Request(
            url=url,
            body=encoded_form_data,
            method="POST",
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'},
            callback=self.parse
        )

    def replace_german_weekdays(self, schedule):
        german_weekdays = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]
        english_weekdays = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]
        for i in range(len(german_weekdays)):
            schedule = schedule.replace(
                german_weekdays[i], english_weekdays[i])
        return schedule

    def parse(self, response):
        json_data = response.json()

        for row in json_data:
            country = row['country_l']
            if (country == 'Österreich'):
                country = 'Austria'
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['sid'],
                'country': country,
                'addr_full': row['town_l'] + ', ' + row['address_l'],
                'phone': row['telnr'],
                'city': row['town_l'],
                'postcode': row['postcode'],
                'lat': row['y'],
                'lon': row['x'],
                'website': 'https://www.omv.at/de-at/tanken/tankstellensuche',
                'opening_hours': self.replace_german_weekdays(row['open_hours']),
            }

            yield GeojsonPointItem(**data)
