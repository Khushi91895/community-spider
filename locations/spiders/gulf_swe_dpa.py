# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import uuid

import re

class GULF_SWE(scrapy.Spider):
    
    name = 'gulf_swe_dpa'
    brand_name = 'Gulf'
    spider_chain_id = "83"
    spider_type = 'chain'
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('swe').alpha_3]
    allowed_domains = ['mgulfsverige.com']

    start_urls = ["https://gulfsverige.com/hitta-station/"]

    def parse(self, response):
        tree = response

        for i in range(50,200):  #id is random, if more gas stations are added, then the range may need to be increased. 73 points are parsed with this range

            title_path = f'//*[@id="post-{i}"]/div[1]/div[2]/header/h2/text()'
            title_nodes = tree.xpath(title_path)
            if (title_nodes):
                title = tree.xpath(title_path)[0].get().strip()

                address_path = f'//*[@id="post-{i}"]/div[1]/div[2]/div[1]/address/span/span[1]/text()'
                address = tree.xpath(address_path)[0].get().strip()

                country_path = f'//*[@id="post-{i}"]/div[1]/div[2]/div[1]/address/span/span[2]/text()'
                country = tree.xpath(country_path)[0].get().strip()

                phone_path = f'//*[@id="post-{i}"]/div[1]/div[2]/div[2]/text()'
                phone = tree.xpath(phone_path).get()

                if (country=='Sverige'):
                    country='Sweden'
                    
                data = {
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'ref': uuid.uuid4().hex,
                    'name': title,
                    'addr_full': address,
                    'country': country,
                    'phone': phone,
                    'website': 'https://gulfsverige.com/hitta-station/',
                }
                yield GeojsonPointItem(**data)
