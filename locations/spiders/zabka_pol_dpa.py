import scrapy

import uuid
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry


class ZabkaStoreSpider(scrapy.Spider):
    name = 'zabka_pol_dpa'
    brand_name = 'Zabka'
    spider_type = "chain"
    spider_chain_id = "5290"
    spider_categories = [Code.CONVENIENCE_STORE.value]
    spider_countries = [pycountry.countries.lookup('pol').alpha_3]
    allowed_domains = ["zabka.pl"]

    # start_urls = ['https://www.zabka.pl/kontakt']

    def start_requests(self):
        yield scrapy.Request(
            url='https://www.zabka.pl/kontakt',
            method='GET',
            callback=self.parse_contact
        )

    def parse_contact(self, response):
        phone = response.xpath(
            '//*[@id="main"]/section/div/div/div[1]/div/div[2]/p[2]/a/text()').get()

        email = response.xpath(
            '//*[@id="main"]/section/div/div/div[1]/div/div[2]/p[3]/a/text()').get()

        yield scrapy.Request(
            url='https://www.zabka.pl/ajax/shop-clusters.json',
            method='GET',
            callback=self.parse,
            cb_kwargs=dict(email=email, phone=phone)
        )

    def parse(self, response, email, phone):
        stores = response.json()

        for store in stores:
            data = {
                'ref': store.get("id"),
                'chain_name': 'Zabka',
                'chain_id': '5290',
                'website': 'https://www.zabka.pl/',
                'phone': phone,
                'email': email,
                'lat': store.get("lat"),
                'lon': store.get("lng"),
            }

            yield GeojsonPointItem(**data)
