import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code


class DominosSpider(scrapy.Spider):
    name = 'dominos_mex_dpa'
    brand_name = "Domino's"
    spider_type = 'chain'
    spider_chain_id = "27851"
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('mx').alpha_3]
    allowed_domains = ["dominos.com.mx"]

    # start_urls = ["https://order.golo01.dominos.com/store-locator-international/locate/store?regionCode=MX&latitude=23.634501&longitude=-102.552784"]
    
    def start_requests(self):
        url = "https://order.golo01.dominos.com/store-locator-international/locate/store?regionCode=MX&latitude=23.634501&longitude=-102.552784"

        headers = {
            'dpz-language': 'en',
            'dpz-market': 'MEXICO'
        }

        yield scrapy.Request(
            url=url,
            method='GET',
            headers=headers,
            callback=self.parse,
        )

    def parse(self, response):
        responseData = response.json()

        for row in responseData['Stores']:
            data = {
                'ref': row['StoreID'],
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': row['StreetName'],
                'city': row['LocationInfo'],
                'state': row['City'],
                'postcode': row['PostalCode'],
                'phone': row['Phone'],
                'website': 'https://www.nuevo.dominos.com.mx',
                'opening_hours': row['HoursDescription'],
                'lat': float(row['Latitude']),
                'lon': float(row['Longitude']),
            }

            yield GeojsonPointItem(**data)
