import scrapy
import pycountry
import json
import uuid
from locations.items import GeojsonPointItem
from locations.categories import Code

class BrewberrysSpider(scrapy.Spider):
    name = "brewberrys_ind_dpa"
    brand_name = 'Brewberrys cafe'
    spider_type = 'chain'
    spider_chain_id = '32634'
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.get(alpha_2='IN').alpha_2]
    allowed_domains = ['brewberrys.com']
    
    start_urls = ["https://www.brewberrys.com/locator"]
    
    def parse(self, response):
        for location_div in response.css('div.locator-section'):
            location = {}
            location_address_detail = location_div.css('div.location-address-detail p::text').getall()
            location_time = location_div.css('div.location-time h4::text').get()

            location['Address'] = "\n".join(address.strip() for address in location_address_detail) if location_address_detail else ""
            location['Time'] = location_time.strip() if location_time else ""

            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': location['Address'],
                'website': "https://www.brewberrys.com",
                'opening_hours': location_time,
            }

            yield GeojsonPointItem(**data)


