import scrapy
import pycountry
import uuid
from locations.items import GeojsonPointItem
from locations.categories import Code


class BankOfIndiaSpider(scrapy.Spider):
    name = 'bank_of_india_dpa'
    brand_name = 'Bank of India'
    spider_type = 'chain'
    spider_chain_id = '2495'
    spider_categories = [Code.BANK.value]
    spider_countries = [pycountry.countries.lookup('IN').alpha_2]
    allowed_domains = ["bankofindia.co.in"]

    start_urls = ['https://bankofindia.co.in/customer-care?p_p_id=com_boi_locate_us_BoiLocateUsPortlet_INSTANCE_wpfx&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_cacheability=cacheLevelPage&_com_boi_locate_us_BoiLocateUsPortlet_INSTANCE_wpfx_CMD=branch']

    def parse(self, response):
        responseData = response.json()

        for location in responseData:
            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': location['addressone'],
                'postcode': location['pincode'],
                'email': location['email'],
                'phone': location['phoneNumber'],
                'website': 'https://bankofindia.co.in/',
                'lat': location['lat'],
                'lon': location['lng'],
            }
            yield GeojsonPointItem(**data)
