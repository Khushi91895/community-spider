import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code


class BASKINROBBINS_IND(scrapy.Spider):
    name = "baskin_robbins_ind_dpa"
    brand_name = 'Baskin-Robbins'
    spider_type = 'chain'
    spider_chain_id = "286"
    spider_categories = [Code.RESTAURANT.value]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]
    min_lat, max_lat = 8.064675, 37.084107
    min_lng, max_lng = 68.092877, 97.395357
    step = 0.5

    def start_requests(self):
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step) + 1):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step) + 1):
                latitude = lat * self.step
                longitude = lng * self.step
                url = f"https://stockist.co/api/v1/u11410/locations/search?tag=u11410&latitude={latitude}&longitude={longitude}&filter_operator=and&distance=1966.6160336694231&_=st_ipsfmy1309kdl9h6hhzzt7"
                yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        json_data = response.json()['locations']

        for row in json_data:
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['id'],
                'country': row['country'],
                'state': row['state'],
                'addr_full': row['address_line_1'],
                'phone': row['phone'],
                'email': row['email'],
                'city': row['city'],
                'postcode': row['postal_code'],
                'lat': row['latitude'],
                'lon': row['longitude'],
                'website': 'https://baskinrobbinsindia.com',
            }

            yield GeojsonPointItem(**data)