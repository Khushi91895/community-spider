import scrapy
import re
import pycountry
from uuid import uuid4
from locations.items import GeojsonPointItem
from locations.categories import Code

class KFC_CHE(scrapy.Spider):
    name = 'kfc_che_dpa'
    brand_name = 'KFC'
    spider_type = 'chain'
    spider_chain_id = '1559'
    allowed_domains = ['kfc-suisse.ch']
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('Switzerland').alpha_3]

    start_urls = ['https://www.kfc-suisse.ch/api/restaurant/list']

    def parse(self, response):
        json_response: dict = response.json()
        list_of_shops: list[dict] = json_response['itemListElement']

        for shop in list_of_shops:
            data: dict = shop['item']

            opening_hours: list = data['openingHours'].split(',')
            german_addr_to_eng: dict = {
                'Do': 'Th',    
                'So': 'Su',    
                '+': '-'      
            }

            timings: list = []

            for each_day in opening_hours:
                timing_str: str = each_day
                for key in german_addr_to_eng:
                    if key in timing_str:
                        timing_str = timing_str.replace(key, german_addr_to_eng[key])
                timings.append(timing_str)
            opening_hours: str = '; '.join(timings)

            data = {
                'ref': uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'lat': data['geo']['latitude'],
                'lon': data['geo']['longitude'],
                'street': data['location']['streetAddress'],
                'city': data['location']['addressLocality'],
                'postcode': data['location']['postalCode'],
                'phone': data['telephone'].replace(' ', ''),
                'email': data['email'] or '',
                'opening_hours': opening_hours,
            }

            yield GeojsonPointItem(**data)
