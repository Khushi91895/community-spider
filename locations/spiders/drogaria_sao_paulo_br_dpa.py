# -*- coding: utf-8 -*-

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import re

class Drogaria_Sao_Paulo_BR_Spider(scrapy.Spider):

    name = "drogaria_sao_paulo_br_dpa"
    brand_name = "Drogaria São Paulo"
    spider_type = "chain"
    spider_chain_id = "5498"
    spider_categories = [Code.DRUGSTORE_OR_PHARMACY.value, Code.PHARMACY.value]
    spider_countries = [pycountry.countries.lookup('br').alpha_3]
    allowed_domains = ['www.drogariasaopaulo.com.br']

    start_urls = ['https://www.drogariasaopaulo.com.br/api/dataentities/PR/documents/f52e9e7f-a02c-11ea-8337-0a8ac637298d/arquivo/attachments/nossas-lojas.js']

    def parse_hours(self,row):
        
        opening = ''

        if row.get("horarioAberturaSegsex"):
            opening += f'Mo-Fr {row["horarioAberturaSegsex"]}-{row["horarioFechamentoSegsex"]}; '

        if row.get("horarioAberturaSabado"):
            opening += f'Sa {row["horarioAberturaSabado"]}-{row["horarioFechamentoSabado"]}; '

        if row.get("horarioAberturaDomingo"):
            opening += f'Su {row["horarioAberturaDomingo"]}-{row["horarioFechamentoDomingo"]}; '
        
        return opening[:-2]

    def parse_phones(self,row):

        phones = []

        if row.get('telefoneUm'): phones.append(row['telefoneUm'])
        if row.get('telefoneDois'): phones.append(row['telefoneDois'])
        if row.get('whatsapp'): phones.append(row.get('whatsapp'))
        
        return phones

    def parse(self, response):

        responseData = response.json()

        for row in responseData["retorno"]:
    
            data = {
                'ref': row.get('id'),
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': f'{row.get("cidade")}, {row.get("endereco")}, {row.get("uf")}, {row.get("cep")}',
                'city': row.get('cidade'),
                'state': row.get('uf'),
                'street': row.get('endereco'),
                'postcode': row.get('cep'),
                'country': self.spider_countries,
                'phone': self.parse_phones(row),
                'website': 'https://www.drogariasaopaulo.com.br',
                'opening_hours': self.parse_hours(row),
                'lat': float(re.sub(r',', '.', row.get('latitude'))) if row.get('latitude') else "",
                'lon': float(re.sub(r',', '.', row.get('longitude'))) if row.get('longitude') else "",
            }

            yield GeojsonPointItem(**data)