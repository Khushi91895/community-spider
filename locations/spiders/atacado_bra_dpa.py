import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import re


class AtacadoBraSpider(scrapy.Spider):
    name = "atacado_bra_dpa"
    brand_name = "ATACADAO"
    spider_type = 'chain'
    spider_chain_id = '27047'
    spider_categories = [Code.GROCERY.value]
    spider_countries = [pycountry.countries.lookup('br').alpha_3]
    allowed_domains = ['www.atacadao.com.br']

    # start_urls = ['http://www.atacadao.com.br']

    def start_requests(self):
        yield scrapy.Request(
            url="https://apis.cotabest.com.br/folhetos/stores?latitude=-23.56571961652763&longitude=-46.651259360931014",
            method='GET',
            callback=self.parse
        )

    def parse_phone(self, phones: List[Dict[str, str]]):
        contacts = []
        for contact in phones:
            contacts.append(contact["contact"])

        contacts = list(map(lambda phone: "".join(re.findall(r'\d', phone)), contacts))
        return contacts
    

    def parse_opening_hours(self, hours: List[Dict[str, any]]):
        days_portugues: Dict[str, str] = {
            "segunda": "Mo",
            "terça": "Tu",
            "quarta": "We",
            "quinta": "Th",
            "sexta": "Fr",
            "sábado": "Sa",
            "domingo": "Su",
            
            # Only latin symbols in Tuesday and Saturday.
            "terca": "Tu",
            "sabado": "Sa",

            # For Upper-case
            "SEG": "Mo",
            "TER": "Tu",
            "QUA": "We",
            "QUI": "Th",
            "SEX": "Fr",
            "SÁB": "Sa",
            "DOM": "Su",

            # For Lower-case
            "seg": "Mo",
            "ter": "Tu",
            "qua": "We",
            "qui": "Th",
            "sex": "Fr",
            "sáb": "Sa",
            "dom": "Su",

            # For incorrect op_h_marker in web-page.
            "-s:": "-Sa:",
            "SAB": "Sa",
            "sab": "Sa",
            # "": "",
        }
        
        res_op_hours = []

        for hour in hours:
            if hour["type"] == "domingo":
                day = hour["type"]
                day = days_portugues[day]
                
                op = hour["opening"]
                cl = hour["closure"]
                if op is not None or cl is not None:
                    time = f"{op[:-3]}-{cl[:-3]}"
                else:
                    time = "24/7"

                first_type = f"{day} {time}"
                res_op_hours.append(first_type)
            elif hour["type"] == "segunda-a-sabado":
                day: str = hour["type"]
                day = day.replace("-a-", "-")
                day = day.replace("segunda", "Mo")
                day = day.replace("sabado", "Sa")

                op = hour["opening"]
                cl = hour["closure"]
                if op is not None or cl is not None:
                    time = f"{op[:-3]}-{cl[:-3]}"
                else:
                    time = "24/7"

                second_type = f"{day} {time}"
                res_op_hours.append(second_type)
            else:
                day: str = hour["type"]
                day = day.replace("-a-", "-")

                name_days_in_portugese = {
                    "segunda": "Mo",
                    "terça": "Tu",
                    "quarta": "We",
                    "quinta": "Th",
                    "sexta": "Fr",
                    "sábado": "Sa",
                    "domingo": "Su",
                    
                    # Only latin symbols in Tuesday and Saturday.
                    "terca": "Tu",
                    "sabado": "Sa",
                }

                for _day in name_days_in_portugese:
                    day.replace(_day, name_days_in_portugese[_day])

                op = hour["opening"]
                cl = hour["closure"]
                if op is not None or cl is not None:
                    time = f"{op[:-3]}-{cl[:-3]}"
                else:
                    time = "24/7"

                third_type = f"{day} {time}"
                res_op_hours.append(third_type)
    
        res_op_hours = "; ".join(res_op_hours)

        return res_op_hours


    def parse_address(self, addr_data):
        if addr_data is not None:
            data = {
                "city": addr_data["city"]["name"],
                "street": addr_data["street"],
                "state": addr_data["city"]["state"]["name"],
            }
        else:
            data = {
                "city": "",
                "street": "",
                "state": "",
            }

        return data

    def parse(self, response):
        responseData = response.json()

        for store in responseData["data"]:
            addr_data = self.parse_address(store["store"]["address"])
            data = {
                'ref': store["store"]["id"],
                'city': addr_data["city"],
                'street': addr_data["street"],
                'state': addr_data["state"],
                'country': pycountry.countries.lookup('br').official_name,
                'opening_hours': self.parse_opening_hours(store["store"]["opening_hours"]),
                'website': 'https://www.atacadao.com.br/',
                'phone': self.parse_phone(store["store"]["contacts"]),
                'lat': store["store"]["coordinates"]["latitude"],
                'lon': store["store"]["coordinates"]["longitude"],
                'chain_id': self.spider_chain_id,
                'chain_name': self.brand_name,
            }

            yield GeojsonPointItem(**data)
    