import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class SparDK(scrapy.Spider):
    name = "spar_dk_dpa"
    brand_name = 'Spar'
    spider_type = 'chain'
    spider_chain_id = "1087"
    spider_categories = [Code.CONVENIENCE_STORE.value]
    spider_countries = [pycountry.countries.lookup('dk').alpha_3]
    min_lat, max_lat = 46.4000001, 49.0200001
    min_lng, max_lng = 9.5300001, 17.1500001
    step = 0.02

    # start_urls = ["https://spar.dk"]

    def start_requests(self):
        base_url = "https://spar.dk/search"
        yield scrapy.Request(
            url=base_url,
            headers={
                "accept": "application/json, text/plain, */*",
                "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                "cache-control": "no-cache",
                "content-type": "application/json",
                "pragma": "no-cache",
                "sec-ch-ua": "\"Google Chrome\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"Windows\"",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "cookie": "CookieInformationConsent=%7B%22website_uuid%22%3A%221826f0db-7484-49a0-935f-e30eecf1cf83%22%2C%22timestamp%22%3A%222023-06-01T18%3A35%3A40.201Z%22%2C%22consent_url%22%3A%22https%3A%2F%2Fspar.dk%2Ffind-butik%22%2C%22consent_website%22%3A%22spar.dk%22%2C%22consent_domain%22%3A%22spar.dk%22%2C%22user_uid%22%3A%221cb490aa-e34c-46bb-bacc-d127470b5864%22%2C%22consents_approved%22%3A%5B%22cookie_cat_necessary%22%2C%22cookie_cat_functional%22%2C%22cookie_cat_statistic%22%2C%22cookie_cat_marketing%22%2C%22cookie_cat_unclassified%22%5D%2C%22consents_denied%22%3A%5B%5D%2C%22user_agent%22%3A%22Mozilla%2F5.0%20%28Windows%20NT%2010.0%3B%20Win64%3B%20x64%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F113.0.0.0%20Safari%2F537.36%22%7D; _gid=GA1.2.1991204600.1685644540; _clck=zj9eru|2|fc3|0|1247; 111514-sid=53adfded-ddf7-4aaf-941c-378e884c57d6; mAudiences=%7B%22m%22%3A%224112833743683206506%22%2C%22a%22%3A%5B%7B%22audience_id%22%3A2742%7D%5D%7D; 111514-sid-seen=1685644667940; lf-cmp-111514={\"displayCount\":2,\"lastSeen\":1685644666,\"ended\":true,\"ended_due_to\":\"remove_teaser_when_popup_close\",\"ended_on\":1685644670}; _gat_UA-6736379-12=1; _ga_KT7RQGEJS6=GS1.1.1685644545.1.1.1685644771.0.0.0; _ga_VRT19R3GKS=GS1.1.1685644535.1.1.1685644773.3.0.0; _ga=GA1.1.1557771353.1685644538; _ga_JT95FG7RM6=GS1.1.1685644536.1.1.1685644773.0.0.0; mprtcl-v4_8BD8732C={'gs':{'ie':1|'dt':'eu1-d336073821c2774f9e3fd0f264d7990b'|'cgid':'55695425-293f-4a0b-60fc-20507b8d8243'|'das':'d79344a1-31d2-4cfc-e4ca-ea1ffdeb7240'|'csm':'WyI0MTEyODMzNzQzNjgzMjA2NTA2Il0='|'sid':'66C468D9-D4AF-4D99-6CAF-2BEEE4EF91E6'|'les':1685644773196|'ssd':1685644541312}|'l':0|'4112833743683206506':{'fst':1685644541928|'con':'eyJnZHByIjp7Im1hcmtldGluZyI6eyJjIjp0cnVlLCJ0cyI6MTY4NTY0NDc3MzE4MiwiZCI6IiIsImwiOiJodHRwczovL3NwYXIuZGsvZmluZC1idXRpayIsImgiOiJNUElEOjQxMTI4MzM3NDM2ODMyMDY1MDYifSwic3RhdGlzdGljcyI6eyJjIjp0cnVlLCJ0cyI6MTY4NTY0NDc3MzE4MSwiZCI6IiIsImwiOiJodHRwczovL3NwYXIuZGsvZmluZC1idXRpayIsImgiOiJNUElEOjQxMTI4MzM3NDM2ODMyMDY1MDYifX19'|'ua':'eyJtb3N0X3JlY2VudF9PUyI6IldpbmRvd3MifQ=='}|'cu':'4112833743683206506'}; _clsk=cr0cs6|1685644773633|5|1|x.clarity.ms/collect",
                "Referer": "https://spar.dk/find-butik",
                "Referrer-Policy": "strict-origin-when-cross-origin"
            },
            body=json.dumps({
                "params": {"wt": "json"}, 
                "filter": [], 
                "query": "ss_search_api_datasource:\"entity:node\" AND bs_status:true AND ss_type:\"store\"", "limit": 1000
                }),
            method="POST",
            callback=self.parse)

    def parse_opening_hours(self, opening_hours):
        days = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
        parsed_hours = []

        try:
            opening_hours = json.loads(opening_hours)
        except json.JSONDecodeError:
            return "Invalid opening hours format"

        for entry in opening_hours:
            day = days[entry['day']]
            start_hours = entry['starthours'] // 100
            start_minutes = entry['starthours'] % 100
            end_hours = entry['endhours'] // 100
            end_minutes = entry['endhours'] % 100

            opening_hours_str = f"{day}: {start_hours:02d}:{start_minutes:02d}-{end_hours:02d}:{end_minutes:02d}"
            parsed_hours.append(opening_hours_str)

        return "; ".join(parsed_hours)

    def parse(self, response):
        json_data = response.json()['response']['docs']
        for row in json_data:
            lat, lng = row['locs_latlon'].split(",")
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['its_nid'],
                'addr_full': ' '.join(row['tm_X3b_en_locality']) + ', ' + ' '.join(row['tm_X3b_en_address_line1']),
                'city': ''.join(row['tm_X3b_en_locality']),
                'lat': lat,
                'lon': lng,
                'website': 'https://www.spar.dk',
                'store_url': 'https://www.spar.dk' + row['ss_path_alias'],
                'postcode': ''.join(row['tm_X3b_en_postal_code']),
                'phone': row['ss_field_store_phone'],
                "opening_hours": self.parse_opening_hours(row['sm_solr_opening_hours'][0]),
            }
            yield GeojsonPointItem(**data)
