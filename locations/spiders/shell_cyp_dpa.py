# -*- coding: utf-8 -*-

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import uuid

class ShellCYSpider(scrapy.Spider):
    name = 'shell_cyp_dpa'
    brand_name = 'Shell'
    spider_type = 'chain'
    spider_chain_id = "10"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('cy').alpha_3]
    allowed_domains = ['coralenergy.cy']

    # start_urls = ['https://shell.com']

    def start_requests(self):
        url = "https://www.coralenergy.com.cy/umbraco/api/NetworkDisplay/GetPoints/"
        payload = "{\"Key\":6779}"
        headers = {
            'content-type': 'application/json; charset=UTF-8',
            'x-requested-with': 'XMLHttpRequest'
        }        
        yield scrapy.Request(
            method='POST',
            url=url,
            headers=headers,
            body=payload
        )


    def parse(self, response):
        '''
        30 Features (2022-06-23)
        '''

        responseData = response.json()['points']
        for i, row in enumerate(responseData):
            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'name': row['title'],
                'addr_full': row['address'],
                'phone': row['telephones'],
                'lat': row['latitude'],
                'lon': row['longitude']
            }

            yield GeojsonPointItem(**data)