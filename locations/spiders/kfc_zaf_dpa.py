import scrapy
import re
import json
import pycountry
import uuid
from locations.items import GeojsonPointItem
from locations.categories import Code

class KFC_zaf_dpa(scrapy.Spider):
    name ='kfc_zaf_dpa'
    brand_name = 'KFC'
    spider_type = 'chain'
    spider_chain_id = '1559'
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('South Africa').alpha_3]
    allowed_domains = ['kfc.co.za']

    start_urls = ['https://order.kfc.co.za/ourrestaurants']

    def parse(self, response):
        '''
        @url https://order.kfc.co.za/ourrestaurants
        @returns requests 0 0
        @returns items 509 509
        @scrapes ref name addr_full postcode city state lat lon phone opening_hours extras
        '''
        
        items = response.xpath("//script[starts-with(normalize-space(), 'var allRestDetails')]").get()
        items = re.search("\\'(\\[.*\\])\\';", items, re.MULTILINE).group(1)
        items = json.loads(items)
        
        for item in items:
            phone: list[str] = [item['PhoneNo']]

            if item['Additional_PhoneNo']:
                phone.append(item['Additional_PhoneNo'])

            days: tuple[str] = ('Mo-Th', 'Fr', 'Sa', 'Su')
            opening_hours: list = []

            item['Hours']: list
            time: dict
            day: str
            for time, day in zip(item['Hours'], days):
                opening_hours.append(f'{day} {time["OpenTime"]}-{time["CloseTime"]}')

            opening_hours: str = '; '.join(opening_hours)

            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': f'{item["AddressLine1"]} {item["AddressLine2"]}'.strip(),
                'postcode': item['PostalCode'],
                'city': item['City'],
                'state': item['Province'],
                'lat': item['Lat'],
                'lon': item['Long'],
                'phone': phone,
                'opening_hours': opening_hours,
            }

            yield GeojsonPointItem(**data)
