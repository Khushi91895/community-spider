import scrapy
import pycountry
import json
import uuid
import re
from locations.items import GeojsonPointItem
from locations.categories import Code

class CoffeblackMexSpider(scrapy.Spider):
    name = "coffeblack_mex_dpa"
    brand_name = 'Black Coffee Gallery'
    spider_type = 'chain'
    spider_chain_id = "22476"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('MX').alpha_3]
    allowed_domains = ['www.blackcoffeegallery.com.mx/']

    start_urls = ['https://www.blackcoffeegallery.com.mx/wp-admin/admin-ajax.php?action=asl_load_stores&nonce=99bd54a92b&load_all=1&layout=1']

    def parse(self, response):
        json_data = json.loads(response.body)
        
        for row in json_data:
            street = None
            
            if isinstance(row.get('street'), dict):
                street = row.get('street', {}).get('state', {}).get('postal_code')
            city = row.get('city')
            state = row.get('state')
            postal_code = row.get('postal_code')

            addr_parts = [part for part in [street, city, state, postal_code] if part is not None]
            addr_full = ', '.join(str(part) for part in addr_parts)
            open1 = row.get('open_hours')
            open1 = open1.replace("{", "").replace("}", "").replace("[", "").replace("]", "")

        
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': uuid.uuid4().hex,
                'city': city,
                'state': state,
                'country': row.get('country'),
                'addr_full': addr_full,
                'phone': row.get('phone'),
                'opening_hours':open1,   
                'lat': row.get('lat'),
                'lon': row.get('lng'),
                'website': 'https://www.blackcoffeegallery.com.mx',
                'email': row.get('email')
            }
            yield GeojsonPointItem(**data)