import uuid
import scrapy
import re
import pycountry
from scrapy.responsetypes import Response
from locations.categories import Code
from locations.items import GeojsonPointItem


class FnbZafSpider(scrapy.Spider):
    name = "fnb_zaf_dpa"
    brand_name = "FNB"
    spider_type = "chain"
    spider_chain_id = "33979"
    spider_categories = [
        Code.BANK.value,
        Code.ATM.value
        ]
    spider_countries = [
        pycountry.countries.lookup('zaf').alpha_3,
        ]
    allowed_domains = ["www.fnb.co.za"]
    
    # start_urls = ["https://www.fnb.co.za/Controller?nav=locators.BranchLocatorSearch&branchName=&booking=true"]

    def start_requests(self):
        url = "https://www.fnb.co.za/Controller?nav=locators.BranchLocatorSearch&branchName=&booking=true"
        
        yield scrapy.Request(
            url=url,
            method='POST',
            callback=self.parse_branch
            )

    def parse_branch(self, response: Response):
        event = map(lambda x: x.replace(';', ''), response.css(
            'a[href="#"]::attr(onclick)').getall())
        links = map(lambda x: re.match(
            "loadPanel\\(event,'(.+)'\\)", x).group(1), event)
        for link in links:
            yield scrapy.Request(url=f'https://www.fnb.co.za{link}', callback=self.parse, method='POST')

    def parse(self, response: Response):
        data = {
            'ref': uuid.uuid4().hex,
            'chain_id': self.spider_chain_id,
            'chain_name': self.brand_name,
            'addr_full': response.css('p strong::text').get().strip(),
            'phone': response.css('p.linePadding strong::text').getall()[1].strip(),
            'email': response.css('p.linePadding strong a::text').getall()[0].strip(),
            'opening_hours': ', '.join(list(self.parse_hours(response))),
            'website': 'https://www.fnb.co.za',
            'lat': response.css('p.fontGrey::text').getall()[3].strip(),
            'lon': response.css('p.fontGrey.linePadding::text').getall()[1].strip(),
        }
        yield GeojsonPointItem(**data)

    def parse_hours(self, response: Response):
        schedule = {}

        for loc in range(11, 18):
            section = response.css(
                f'body > div.overlayPanel.dynBuiltPanel.overlayPanelHide > div > div > div.wrapper > div:nth-child(4) > div > div > div:nth-child(2) > div > div:nth-child({loc})')
            day_name = section.css('label::text').get().strip()
            time = section.css('div p strong::text').get().replace(
                ' to ', '-').strip()

            if time in schedule.keys():
                schedule[time].append(day_name)
                continue

            schedule[time] = [day_name]

        for time, day in schedule.items():
            if time == 'Closed':
                yield f'{day[0][:2]} off'
                continue
            if len(day) == 1:
                yield f'{day[0][:2]} {time}'
                continue  # converting time to openstreetmap format
            yield f'{day[0][:2]}-{day[-1][:2]} {time}'
