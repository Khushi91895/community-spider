# -*- coding: utf-8 -*-

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code

class DunkinUSSpider(scrapy.Spider):
    # REQUIRES US VPN
    name = "dunkin_us_dpa"
    brand_name = "Dunkin'"
    spider_type = "chain"
    spider_chain_id = "33502"
    spider_categories = [Code.DOUGHNUT_SHOP.value]
    spider_countries = [pycountry.countries.lookup('usa').alpha_3]
    allowed_domains = ["dunkindonuts.com"]

    # start_urls = "https://www.dunkindonuts.com/bin/servlet/dsl"

    def start_requests(self):
        url = "https://www.dunkindonuts.com/bin/servlet/dsl"

        payload='service=DSL&origin=38.012%252C-101.164&radius=1000000&maxMatches=1000000&pageSize=1&units=m&ambiguities=ignore'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': 'AWSALB=XxyIwBeLPzZgrV+Phf2hyMPGmTpaDrZPgKfBgr8bf1tZgp4+vl6nTdctcmCGTI8vdb9wZ2DMSVK+SN/qZwxbLaLfVWt66NJD3+OQAVS+mYJc1Ob/hq3l2QUMkbt7; AWSALBCORS=XxyIwBeLPzZgrV+Phf2hyMPGmTpaDrZPgKfBgr8bf1tZgp4+vl6nTdctcmCGTI8vdb9wZ2DMSVK+SN/qZwxbLaLfVWt66NJD3+OQAVS+mYJc1Ob/hq3l2QUMkbt7'
        }

        yield scrapy.Request(
            method='post',
            url=url,
            headers=headers,
            body=payload
        )


    def parse(self, response):
        '''
        @url https://www.dunkindonuts.com/bin/servlet/dsl
        @returns 9515 9525
        @scrapes ref website brand addr_full postcode city state country phone opening_hours lat lon
        '''

        responseData = response.json()['data']['storeAttributes']

        # Parse opening hours
        dayKeys = ['mon_hours', 'tue_hours', 'wed_hours', 'thu_hours', 'fri_hours', 'sat_hours', 'sun_hours']
        for row in responseData:    
            op = ''
            for day in dayKeys:
                # First letter of key capital
                # second lower
                if row[day] != '':
                    op += f"{day[0].upper()}{day[1]} {row[day]}; "

        
            data = {
                'ref': row['recordId'],
                'chain_id': "33502",
                'chain_name': "Dunkin'",
                'addr_full': row['address'],
                'postcode': row['postal'],
                'city': row['city'],
                'state': row['state'],
                'country': row['country'],
                'phone': row['phonenumber'].replace('-',''),
                'opening_hours': op,
                'website': 'https://www.dunkindonuts.com',
                'lat': float(row['lat']),
                'lon': float(row['lng']),
            }

            yield GeojsonPointItem(**data)