import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from datetime import datetime, timedelta


class StarbucksNLD(scrapy.Spider):
    name = "starbucks_nld_dpa"
    brand_name = 'Starbucks'
    spider_type= 'chain'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('nld').alpha_3]
    min_lat, max_lat = 50.7504000, 53.5594000
    min_lng, max_lng = 3.0754000, 7.1089000
    step = 0.1

    # start_urls = ["https://www.starbucks.nl"]

    def start_requests(self):
        base_url = "https://www.starbucks.nl/nl/api/v1/store-finder?latLng="
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                if (self.is_inside_norway_polygon(lat * self.step, lng * self.step)):
                    url = base_url + f"{lat * self.step}%2C{lng * self.step}"
                    yield scrapy.Request(url=url, callback=self.parse)

    def is_inside_norway_polygon(self, lat, lng):
        nld_hexagon_vertices = [
            (53.505381, 6.176090),
            (53.531131, 5.956137),
            (53.423303, 4.994652),
            (51.620647, 3.494927),
            (51.541640, 3.882246),
            (51.374984, 4.313425),
            (51.516378, 5.134797),
            (51.678200, 5.865175),
            (51.837955, 5.861317),
            (52.087256, 6.327913),
            (52.256810, 5.779245),
            (52.331126, 6.042880),
            (52.470919, 5.991170),
            (52.558578, 6.913918),
            (53.039270, 7.221000),
        ]

        n = len(nld_hexagon_vertices)
        inside = False
        p1x, p1y = nld_hexagon_vertices[0]
        for i in range(n + 1):
            p2x, p2y = nld_hexagon_vertices[i % n]
            if lng > min(p1y, p2y):
                if lng <= max(p1y, p2y):
                    if lat <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (lng - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                        if p1x == p2x or lat <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
            if row['description'] != 'Closed':
                data = {
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'ref': row['id'],
                    'addr_full': row['address'],
                    'phone': row['phoneNumber'],
                    'lat': float(row['coordinates']['lat']),
                    'lon': float(row['coordinates']['lng']),
                    'website': 'https://www.starbucks.nl/nl/store-locator?types=starbucks',
                    "opening_hours": '',
                }
                days = {
                    'Monday': 'Mo',
                    'Tuesday': 'Tu',
                    'Wednesday': 'We',
                    'Thursday': 'Th',
                    'Friday': 'Fr',
                    'Saturday': 'Sa',
                    'Sunday': 'Su',
                }

                opening_hours = ''

                for hours in row['hoursNext7Days']:
                    name = hours['name']
                    if name == 'Today':
                        name = self.get_day_abbr()
                    elif name == 'Tomorrow':
                        name = self.get_day_abbr(0)
                    else:
                        name = days.get(name, '')
                    if (name and hours['description'] != 'Closed'):
                        start_time, end_time = hours['description'].split(' to ')
                        start_time = datetime.strptime(start_time, '%I:%M %p').strftime('%H:%M')
                        end_time = datetime.strptime(end_time, '%I:%M %p').strftime('%H:%M')
                        opening_hours += f'{name} {start_time}-{end_time}; '

                data['opening_hours'] = opening_hours.strip()
                yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=-1):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days]
