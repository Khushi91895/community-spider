import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class GulfMex(scrapy.Spider):
    
    name = "gulf_mex_dpa"
    brand_name = 'Gulf'
    spider_type = 'chain'
    spider_chain_id = "83"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('mex').alpha_3]

    # start_urls = ["https://gulfmexico.mx"]

    def start_requests(self):
        url = "https://b2c.gulfapps.mx/api/stations?access_token=bCusmXnf8U4drQdGXi3YaNEWAHvohGI0gUb3KnM18TOmk71I3AaNLHmzD5Jrokmx"
        yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        json_data = response.json()
        for row in json_data:
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['stationKey'],
                'addr_full': row['vaddress'],
                'street': row['stationAddress']['street'],
                'website': 'https://gulfmexico.mx/locations/',
                'postcode': row['stationAddress']['zipCode'],
                'phone': row['phone'],
                'lat': row['coordinates']['lat'],
                'lon': row['coordinates']['lng'],
            }
            yield GeojsonPointItem(**data)