import scrapy
from locations.categories import Code
import pycountry
import uuid
from locations.items import GeojsonPointItem


class LojasAmericanasSpider(scrapy.Spider):
    name = 'lojas_americanas_dpa'
    brand_name = 'Lojas Americanas'
    spider_type = 'chain'
    spider_chain_id = "22822"
    spider_categories = [Code.DEPARTMENT_STORE.value]
    spider_countries = [pycountry.countries.lookup('br').alpha_3]
    allowed_domains = ['nossaslojas.americanas.com.br']
    start_urls = ['https://nossaslojas.americanas.com.br/static/json/lojas_mapahome.json']

    def parse(self, response):
        '''
        @url https://nossaslojas.americanas.com.br/static/json/lojas_mapahome.json
        @returns items 1700 1800
        @scrapes lat lon
        '''
        responseData = response.json()
        
        for item in responseData:
            store = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'website': 'https://nossaslojas.americanas.com.br/',
                'lat': item.get('Latitude'),
                'lon': item.get('Longitude'),
            }
            
            yield GeojsonPointItem(**store)
