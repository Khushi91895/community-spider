import scrapy
import pycountry
from locations.categories import Code
from locations.items import GeojsonPointItem
import uuid


class US_POLO_IND(scrapy.Spider):
    name = "us_polo_ind_dpa"
    brand_name = 'U.S. Polo'
    spider_type = 'chain'
    spider_chain_id = "7641"
    spider_categories = [Code.CLOTHING_AND_ACCESSORIES.value]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]

    start_urls = ["https://uspoloassnindia.com/stores/country/IN/lang/en"]

    def parse(self, response):

        json_data = response.json()['response']['entities']

        for row in json_data:
            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'city': row['address']['city'],
                'addr_full': row['address']['city'] + ', ' + row['address']['line1'],
                'country': row['address']['countryCode'],
                'state': row['address']['region'],
                'postcode': row['address']['postalCode'],
                'lat': row['serviceDisplayCoordinate']['latitude'],
                'lon': row['serviceDisplayCoordinate']['longitude'],
                'website': 'https://uspoloassnindia.com/store-locator',
            }
            yield GeojsonPointItem(**data)