import scrapy
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List
import pycountry
import re

class BanregioSpider(scrapy.Spider):
    name = 'banregio_mex_dpa'
    brand_name = 'Banregio'
    spider_chain_id = "5127"
    spider_type = 'chain'
    spider_categories = [Code.ATM.value]
    spider_countries = [pycountry.countries.lookup('mex').alpha_3]
    allowed_domains = ['banregio.com']
    
    # start_urls = ['https://www.banregio.com/backend/ubicaciones.php']
    
    def start_requests(self):
        url = "https://www.banregio.com/backend/ubicaciones.php"

        headers = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest'
        }
        
        yield scrapy.Request(
            url, 
            headers=headers,
            method='POST',
            )

    def parse(self, response):
        data = json.loads(response.body)
        seen_addresses = set()
        
        for branch in data:
            if branch.get('TIPO') not in ['S', 'C']:
                continue

            address = branch['DOMICILIO']
            
            if address in seen_addresses:
                continue
            seen_addresses.add(address)
           
            item = GeojsonPointItem()
            item['ref'] = branch['ID']
            item['brand'] = self.brand_name
            item['chain_id'] = self.spider_chain_id
            item['addr_full'] = address
            address_parts = address.split(',')
            
            housenumber_match = re.search(r'\d+[A-Za-z]*', address_parts[0])
            if housenumber_match:
                item['housenumber'] = housenumber_match.group()
            else:
                item['housenumber'] = ""
            
            item['street'] = address_parts[0]
            item['city'] = branch['CIUDAD']
            item['state'] = branch['ESTADO']
            item['postcode'] = address[-5:]
            item['phone'] = branch['TELEFONO']
            item['email'] = branch['CORREO_GERENTE']
            item['website'] = 'https://www.banregio.com/'
            item['opening_hours'] = self.parse_hours(branch['HORARIO'])
            item['lat'] = branch['LATITUD']
            item['lon'] = branch['LONGITUD']
            
            yield item
    
    def parse_hours(self, hours):
        parsed_hours = []

        hour_ranges = hours.split(',')
        for hour_range in hour_ranges:
            if 'de' not in hour_range:
                continue

            days, times = hour_range.split('de', 1)
            days = days.strip()
            times = times.strip()

            time_parts = re.findall(r'\d+:\d+', times)
            if len(time_parts) == 2:
                open_time = time_parts[0]
                close_time = time_parts[1]
                osm_hours = self.transform_to_osm_hours(days, open_time, close_time)
                parsed_hours.append(osm_hours)

        return '; '.join(parsed_hours)

    def transform_to_osm_hours(self, days, open_time, close_time):
        osm_days = self.transform_days_to_osm_format(days)
        osm_hours = f'{osm_days} {open_time}-{close_time}; PH off'
        return osm_hours

    def transform_days_to_osm_format(self, days):
        days_mapping = {
            'Lunes': 'Mo',
            'Martes': 'Tu',
            'Miércoles': 'We',
            'Jueves': 'Th',
            'Viernes': 'Fr',
            'Sábado': 'Sa',
            'Domingo': 'Su'
        }

        transformed_days = [days_mapping[day] for day in days.split(' ') if day in days_mapping]

        return ','.join(transformed_days)


       



