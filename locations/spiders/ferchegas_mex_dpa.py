# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List
import uuid


class FERCHEGAS_MEX(scrapy.Spider):
    
    name = 'ferchegas_mex_dpa'
    brand_name = 'FercheGas'
    spider_chain_id = "33996"
    spider_type = 'chain'
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('mex').alpha_3]
    
    start_urls = ["https://ferchegas.com/nuestras-estaciones"]

    def parse(self, response):
        tree = response
        # the size of the cycles must be chosen manually
        for i in range(1, 4):    # i is the number of rows of blocks
            for j in range(1, 4):  # j is the number of columns of blocks
                for k in range(1, 19):  # k is the number of records in each block
                    title_path = f'/html/body/main/section/div[2]/div[{i}]/div[{j}]/div/details/aside/div[{k}]/p[1]/text()'
                    if (tree.xpath(title_path).get() == None):
                        continue
                    title = tree.xpath(title_path).get()

                    address_path = f'/html/body/main/section/div[2]/div[{i}]/div[{j}]/div/details/aside/div[{k}]/address/text()'
                    address = tree.xpath(address_path).get().replace('\n', ' ').replace(
                        '                                            ', '')

                    phone_path = f'/html/body/main/section/div[2]/div[{i}]/div[{j}]/div/details/aside/div[{k}]/p[2]/text()'
                    if (tree.xpath(phone_path).get()):
                        phone = tree.xpath(
                            phone_path).get().replace('Tel.', '')

                    data = {
                        'chain_name': self.brand_name,
                        'chain_id': self.spider_chain_id,
                        'ref': uuid.uuid4().hex,
                        'addr_full': address,
                        'phone': phone,
                        'website': 'https://ferchegas.com/nuestras-estaciones',
                    }
                    yield GeojsonPointItem(**data)
