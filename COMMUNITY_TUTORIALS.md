# 2021-2023 | Tutorials made by community enthusiasts

|name|title|tutorial_link|
|-|-|-|
|Matvey Kostenko|Extracting information about BP and SHELL gas stations using Scrapy|[link](https://twilty.com/app/viewer/32b134ff-5974-41dc-ad66-be550c562cee)|
|Daria Efimova|Parsing Amen Bank (Tunisia) website|[link](https://twilty.com/app/viewer/285a496d-eaab-4552-b72e-00e872e0049f)|
|Member|Extracting information about Sok stores in Turkey using Scrapy and BeautifulSoup|[link](https://twilty.com/app/viewer/3447f31f-2ffb-409e-b55e-20d6efe48d10)|
|Eugen|KFC CY Parsing|[link](https://twilty.com/app/viewer/b34ec092-ff6e-42c1-b6b1-f8bd49c8e670)|
|Dev Narula|KFC Belgium Parsing|[link](https://twilty.com/app/viewer/7f289697-4cf7-4459-ad5f-557bfece3b7c)|
|Martin Bárta|Parsing SUBWAY Website|[link](https://twilty.com/app/viewer/af952227-fe74-425e-a038-cea63229b27d)|
|Ronit Kumar Gupta|Parsing Boots Pharmacy|[link](https://twilty.com/app/viewer/773e0577-9187-49e8-95b0-1db699c4af29)|
|Denis Krupin|Cacau Show Brasil Parsing|[link](https://twilty.com/app/viewer/5c0c9c55-cb6c-472c-9fb4-c4a0dbcfc333)|
|Nastya Kharina|Elektra MEX|[link](https://twilty.com/app/viewer/7487748b-d2b1-4f4e-92f1-2743da5311d8)|
